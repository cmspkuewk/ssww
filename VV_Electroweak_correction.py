import ROOT
import argparse
#ROOT.ROOT.EnableImplicitMT(8)
ROOT.PyConfig.IgnoreCommandLineOptions = True

parser = argparse.ArgumentParser(description='manual to this script')
parser.add_argument('-o','--output', help='output name', default= 'new.root')
args = parser.parse_args()

VV_EW_correction_cpp = '''
#include "TMath.h"
#include "Math/Vector4D.h"
#include "ROOT/RDataFrame.hxx"
#include "ROOT/RVec.hxx"
#include "TH1D.h"

using namespace ROOT::VecOps;
using rvec_f = const RVec<float>&;
using rvec_i = const RVec<int>&;
using rvec_b = const RVec<bool>&;

TFile VV_EW_correction_file("/afs/cern.ch/work/j/jixiao/polar_condor/VV_NLO_LO_CMS_mjj.root");
TH1D * hWW_KF_CMS = (TH1D*) VV_EW_correction_file.Get("hWW_KF_CMS");
TH1D * hWWQCD_KF_CMS = (TH1D*) VV_EW_correction_file.Get("hWWQCD_KF_CMS");

float EW_correction_weight(int nGenPart, rvec_f GenPart_pt, rvec_f GenPart_eta, rvec_f GenPart_phi, rvec_f GenPart_mass, rvec_i GenPart_pdgId,rvec_i GenPart_statusFlags, int nGenJet,rvec_f GenJet_pt, rvec_f GenJet_eta, rvec_f GenJet_phi, rvec_f GenJet_mass, int err=0)
{
    int ngoodjet=0;
    bool goodjet;
    float mjj=-999;
    RVec<ROOT::Math::PtEtaPhiMVector> parts;

    // select good leptons
    RVec<int> goodleptons;
    for (int i=0; i<nGenPart;i++){
        if ((abs(GenPart_pdgId[i])==13 || abs(GenPart_pdgId[i])==11) && GenPart_pt[i]> 10){ // is lepton, and pt > 10
            if (GenPart_statusFlags[i]>>0 &1 || GenPart_statusFlags[i]>>5 &1 ){ // is prompt
                goodleptons.push_back(i);
            }
        }
    }

    for (int i=0; i<nGenJet;i++){
        goodjet=true;
        // check if this jet is cleaned
        for (int j=0; j<goodleptons.size();j++){
            // conesize 0.4
            if(sqrt(pow(GenJet_eta[i]-GenPart_eta[goodleptons[j]],2)+pow(GenJet_phi[i]-GenPart_phi[goodleptons[j]],2))<0.4){ // not cleaned
                goodjet=false;
                break;
            }

        }
        // save this jet if goodjet
        if (goodjet){
            ROOT::Math::PtEtaPhiMVector p(GenJet_pt[i], GenJet_eta[i], GenJet_phi[i], GenJet_mass[i]);
            parts.push_back(p);
            ngoodjet++;
        }
        // break if get 2 goodjets
        if (ngoodjet>1) break;
    }

    if (ngoodjet>1){
        mjj=(parts[0]+parts[1]).M();
    }else{
        // no genmjj, return 1.
        return 1.;
    }

    TH1D * EW_correction = hWW_KF_CMS;
    TH1D * QCD_correction = hWWQCD_KF_CMS;

    float mymjj_EW = mjj;
    if (mymjj_EW>1975.0) mymjj_EW=1975.0;
    if (mymjj_EW<525.0) mymjj_EW=525.0;
    float nominal_EW_correction = EW_correction->GetBinContent(EW_correction->GetXaxis()->FindFixBin(mymjj_EW));

    float qcd_delta = TMath::Abs(QCD_correction->GetBinContent(QCD_correction->GetXaxis()->FindFixBin(mymjj_EW))-nominal_EW_correction);
    if (err==1){nominal_EW_correction+=qcd_delta;}
    if (err==2){nominal_EW_correction-=qcd_delta;}

    return nominal_EW_correction;
}
'''
# TL [0.0192, 0.0118, 0.0361, 0.0594, 0.0872, 0.1282]
# TT [0.0178, 0.016, 0.0365, 0.0608, 0.0879, 0.1308]
# LL [0.0208, 0.0119, 0.0374, 0.0636, 0.0938, 0.1404]
# mjj: float tmp3[]={500,1000,1500,2000,2700,3500,5000};
QCDScale_uncertainty_cpp = '''
float QCDScale_uncertainty_LL(int nLHEPart,rvec_f LHEPart_pt, rvec_f LHEPart_eta, rvec_f LHEPart_phi, rvec_f LHEPart_mass, rvec_i LHEPart_pdgId, bool err_up=true)
{
    float mjj=-9999.;
    int nj=0;

    RVec<ROOT::Math::PtEtaPhiMVector> parts;
    for (int i=0;i<nLHEPart;i++){
        if(abs(LHEPart_pdgId[i])<7){
            nj++;
            ROOT::Math::PtEtaPhiMVector p(LHEPart_pt[i], LHEPart_eta[i], LHEPart_phi[i], LHEPart_mass[i]);
            parts.push_back(p);
        }
    }
    if (nj<2) return 1.0;
    mjj=(parts[0]+parts[1]).M();

    float variation[]={0.0208, 0.0119, 0.0374, 0.0636, 0.0938, 0.1404};
    float wgt=1;

    if(err_up){
        if (mjj<1000) wgt=1+variation[0];
        else if (mjj>=1000 && mjj<1500) wgt=1+variation[1];
        else if (mjj>=1500 && mjj<2000) wgt=1+variation[2];
        else if (mjj>=2000 && mjj<2700) wgt=1+variation[3];
        else if (mjj>=2700 && mjj<3500) wgt=1+variation[4];
        else if (mjj>=3500) wgt=1+variation[5];
    }else{
        if (mjj<1000) wgt=1-variation[0];
        else if (mjj>=1000 && mjj<1500) wgt=1-variation[1];
        else if (mjj>=1500 && mjj<2000) wgt=1-variation[2];
        else if (mjj>=2000 && mjj<2700) wgt=1-variation[3];
        else if (mjj>=2700 && mjj<3500) wgt=1-variation[4];
        else if (mjj>=3500) wgt=1-variation[5];
    }
    return wgt;
}
float QCDScale_uncertainty_TL(int nLHEPart,rvec_f LHEPart_pt, rvec_f LHEPart_eta, rvec_f LHEPart_phi, rvec_f LHEPart_mass, rvec_i LHEPart_pdgId, bool err_up=true)
{
    float mjj=-9999.;
    int nj=0;

    RVec<ROOT::Math::PtEtaPhiMVector> parts;
    for (int i=0;i<nLHEPart;i++){
        if(abs(LHEPart_pdgId[i])<7){
            nj++;
            ROOT::Math::PtEtaPhiMVector p(LHEPart_pt[i], LHEPart_eta[i], LHEPart_phi[i], LHEPart_mass[i]);
            parts.push_back(p);
        }
    }
    if (nj<2) return 1.0;
    mjj=(parts[0]+parts[1]).M();

    float variation[]={0.0192, 0.0118, 0.0361, 0.0594, 0.0872, 0.1282};
    float wgt=1;

    if(err_up){
        if (mjj<1000) wgt=1+variation[0];
        else if (mjj>=1000 && mjj<1500) wgt=1+variation[1];
        else if (mjj>=1500 && mjj<2000) wgt=1+variation[2];
        else if (mjj>=2000 && mjj<2700) wgt=1+variation[3];
        else if (mjj>=2700 && mjj<3500) wgt=1+variation[4];
        else if (mjj>=3500) wgt=1+variation[5];
    }else{
        if (mjj<1000) wgt=1-variation[0];
        else if (mjj>=1000 && mjj<1500) wgt=1-variation[1];
        else if (mjj>=1500 && mjj<2000) wgt=1-variation[2];
        else if (mjj>=2000 && mjj<2700) wgt=1-variation[3];
        else if (mjj>=2700 && mjj<3500) wgt=1-variation[4];
        else if (mjj>=3500) wgt=1-variation[5];
    }
    return wgt;
}
float QCDScale_uncertainty_TT(int nLHEPart,rvec_f LHEPart_pt, rvec_f LHEPart_eta, rvec_f LHEPart_phi, rvec_f LHEPart_mass, rvec_i LHEPart_pdgId, bool err_up=true)
{
    float mjj=-9999.;
    int nj=0;

    RVec<ROOT::Math::PtEtaPhiMVector> parts;
    for (int i=0;i<nLHEPart;i++){
        if(abs(LHEPart_pdgId[i])<7){
            nj++;
            ROOT::Math::PtEtaPhiMVector p(LHEPart_pt[i], LHEPart_eta[i], LHEPart_phi[i], LHEPart_mass[i]);
            parts.push_back(p);
        }
    }
    if (nj<2) return 1.0;
    mjj=(parts[0]+parts[1]).M();

    float variation[]={0.0178, 0.016, 0.0365, 0.0608, 0.0879, 0.1308};
    float wgt=1;

    if(err_up){
        if (mjj<1000) wgt=1+variation[0];
        else if (mjj>=1000 && mjj<1500) wgt=1+variation[1];
        else if (mjj>=1500 && mjj<2000) wgt=1+variation[2];
        else if (mjj>=2000 && mjj<2700) wgt=1+variation[3];
        else if (mjj>=2700 && mjj<3500) wgt=1+variation[4];
        else if (mjj>=3500) wgt=1+variation[5];
    }else{
        if (mjj<1000) wgt=1-variation[0];
        else if (mjj>=1000 && mjj<1500) wgt=1-variation[1];
        else if (mjj>=1500 && mjj<2000) wgt=1-variation[2];
        else if (mjj>=2000 && mjj<2700) wgt=1-variation[3];
        else if (mjj>=2700 && mjj<3500) wgt=1-variation[4];
        else if (mjj>=3500) wgt=1-variation[5];
    }
    return wgt;
}
'''

ROOT.gInterpreter.Declare(VV_EW_correction_cpp)
ROOT.gInterpreter.Declare(QCDScale_uncertainty_cpp)

sample="LL"
if "_ll" in args.output:
    sample="LL"
elif "_tl" in args.output:
    sample="TL"
elif "_tt" in args.output:
    sample="TT"
df = ROOT.RDataFrame("Events",args.output)
df2=df.Define("EW_weight","EW_correction_weight(nGenPart,GenPart_pt,GenPart_eta,GenPart_phi,GenPart_mass,GenPart_pdgId,GenPart_statusFlags,nGenJet,GenJet_pt,GenJet_eta,GenJet_phi,GenJet_mass,0)")\
    .Define("EW_weight_up","EW_correction_weight(nGenPart,GenPart_pt,GenPart_eta,GenPart_phi,GenPart_mass,GenPart_pdgId,GenPart_statusFlags,nGenJet,GenJet_pt,GenJet_eta,GenJet_phi,GenJet_mass,1)")\
    .Define("EW_weight_do","EW_correction_weight(nGenPart,GenPart_pt,GenPart_eta,GenPart_phi,GenPart_mass,GenPart_pdgId,GenPart_statusFlags,nGenJet,GenJet_pt,GenJet_eta,GenJet_phi,GenJet_mass,2)")\
    .Define("ScaleW_up","QCDScale_uncertainty_"+sample+"(nLHEPart,LHEPart_pt,LHEPart_eta,LHEPart_phi,LHEPart_mass,LHEPart_pdgId,true)")\
    .Define("ScaleW_do","QCDScale_uncertainty_"+sample+"(nLHEPart,LHEPart_pt,LHEPart_eta,LHEPart_phi,LHEPart_mass,LHEPart_pdgId,false)")
df2.Snapshot("Events","new_"+args.output)
print(">>>>>>>>>>>>>>>>>>>> save","new_"+args.output)
